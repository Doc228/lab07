#include <stdio.h> //Лаба №5		23. (**) Визначити, чи є задане ціле число простим.

void first_type(); // Взято три функции, каждая выполняет свое действие.
void second_type();
void third_type();
int main()
{
	first_type();
	second_type();
	third_type();
	return 0;
}
void first_type()
{
	int number = 8, i, b = 0; // number - заданное число
	for (i = 2; i != number; i++) {
		if (number % i == 0) {
			b = b + 1;
		}
	} //Число простое, если b = 0
}
void second_type()
{
	int number = 8, i = 2, b = 0; // number - заданное число
	while (i != number) {
		if (number % i == 0) {
			b = b + 1;
		}
		i++;
	} //Число простое, если b = 0
}
void third_type()
{
	int number = 8, i = 2, b = 0; // number - заданное число
	do {
		if (number % i == 0) {
			b = b + 1;
		}
		i++;
	} while (i != number);
} //Число простое, если b = 0
